# dotfiles
Configuration files I use on various software (mainly CLI)
- VIM (best editor ever) - [website](https://www.vim.org/) with solarized theme [website](https://ethanschoonover.com/solarized/)
- Xdefaults (for various X11 apps including RxVT unicode/xosview/xcalc)
- GMrun (command launcher) [website](https://github.com/wdlkmpx/gmrun)
- Bash (shell for command line - includes powerline setup)
- ZSH (nice shell with tons of features and plugins
   - ZSH autosuggestions [website](https://github.com/zsh-users/zsh-autosuggestions)
   - ZSH navigation tools [website](https://github.com/psprint/zsh-navigation-tools)
   - ZSH History substring search[website](https://github.com/zsh-users/zsh-history-substring-search)
   - Powerline10k [website](https://github.com/romkatv/powerlevel10k) - the powerline script


Enjoy

# Install

Please backup you original ~/.zshrc and ~/.bashrc or don't link whatever you  don't want ;-)

```bash
mkdir ~/git
cd ~/git
git clone git@gitlab.com:arnaud.fortier/dotfiles.git
git clone https://github.com/romkatv/powerlevel10k.git
git clone https://github.com/altercation/solarized.git
# Optionnal, zsh plugins if not packaged
git clone https://github.com/zsh-users/zsh-history-substring-search.git
git clone git@gitlab.com:tchqiq/oh-my-zsh.git
git clone https://github.com/zsh-users/zsh-autosuggestions.git
cd ~
ln -s ~/git/powerlevel10k
ln -s ~/git/dotfiles/.zshrc
ln -s ~/git/dotfiles/.vimrc
ln -s ~/git/dotfiles/.bashrc
...
mkdir .vim
ln -s ~/git/solarized/vim-colors-solarized/colors ~/.vim/
```
