# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
#alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

########################################
# Overload actual commands
alias grep='grep --color=auto'
alias dmesg='dmesg -HL'
alias vi='vim'
alias ls='ls -G --color'
alias sysupgrade='sudo apt update &&sudo apt upgrade'

# New aliases
alias fifo="echo $1 > ~/FIFO"
alias lc="ls -al | tr -s \" \" | cut -d\" \" -f3,4,9 | column -t"
alias ll="ls -altrh | tr -s \" \" | cut -d\" \" -f6,7,8,3,4,5,9 | column -t"
alias learning="whatis \$(compgen -c) 2>/dev/null | sort | less"
alias da='date "+%A, %B %d, %Y [%T]"'
alias horloge="watch -n1 -t \"date '+%a %d %m/%Y - %H:%M:%S' | toilet -f future\""


## FileSystem Aliases ##
alias lsl='ls -lh | ccze -A'          # list files, dirs
alias usage='du -ch | grep total'     # size of current dir
alias dut="du -h -c -d 1"             # lst with size of elements in current dir
alias size='sudo du -hsx * | sort -rh | head -10 | ccze -A'         # list 10 dir/files by size
alias sizevar='sudo du -a /var | sort -n -r | head -n 10 | ccze -A' # list 10 dir by size
alias sizehome="sudo du -a ~/ | sort -n -r | head -n 10 | ccze -A"  # list 10 dir by size
alias lsbig="echo listing files & directories by size | pv -qL 10 && ls -lSrh | ccze -A"    # sort files dirs by size
alias listmod="ls -ltr | ccze -A"     # list modified files
alias listf="ls -F | ccze -A"         # list alphabeticaly
alias findit="sudo find / -name"      # find files
alias upd60="find . -mmin -60"        # updated files currentdir for last 60mins
alias rtupd60=" find / -mtime -1"     # updated files in / for last 60mins

## Network Aliases
alias netlisten='lsof -i -P | grep LISTEN | ccze -A'   #listening ports
alias nstat="netstat -p TCP -eWc | ccze -A"            #netstat
alias nstato="netstat -tuael --numeric-hosts --numeric-ports | ccze -A"   #netstat
alias ports='netstat -tulanp | ccze -A'  #netstat
alias speeddown="echo Displaying Download Speed Graph| pv -qL 10 && speedometer -rx eth0"
alias speedup="echo Displaying Upload Speed Graph | pv -qL 10 && speedometer -tx eth0"
alias speedrw="echo Show R/W speeds | pv -qL 10 && dd bs=1000000 count=100 if=/dev/zero of=testfile & speedometer testfile"

## System Aliases ##
alias h='history | ccze -A'           # Nice history
alias sourcebash="source ~/.bashrc"   # source .bashrc
alias inxif="inxi -F | ccze -A"       # System information
# Drives
alias uuid="echo â†’ Listing this system \($(hostname)\) UUID | pv -qL 10 && ls /dev/disk/by-uuid/ -alh | ccze -A" # UUID list
alias drives="echo â†’ Listing connected drives on \($(hostname)\) | pv -qL 10 && lsblk -f | ccze -A"              # list hdds, uuid's
# Systemd
alias kernelmsg="sudo journalctl -f _TRANSPORT=kernel | ccze -A"                  # kernel messages
alias bootmsg="echo â†’ Boot Messages | pv -qL 10 && sudo journalctl -b | ccze -A"  # boot messages
alias systemdmsg="sudo journalctl -f --since \" `date +\"%Y-%m-%d\"`\" | ccze -A"             # dmesg
alias blame="systemd-analyze blame | ccze -A"                                     # WTF slowed down the boot
alias boot="echo â†’ Boot Time | pv -qL 10 && systemd-analyze | ccze -A"            # Boot time
alias units="echo â†’ Listing Units (Systemd) | pv -qL 10 && systemctl list-units | ccze -A" # List of Units
alias errors="echo â†’ Systemd Journal Errors | pv -qL 10 && journalctl -b -p err | ccze -A" # Errors
# Ram
alias meminfo='echo "RAM Information   " | pv -qL 10 &&free -m -l -t | ccze -A'                    # RAM information
alias psmem='echo "Top Processes accesing RAM  " | pv -qL 10 && ps auxf | sort -nr -k 4 | ccze -A' # Process eating RAM
# CPU
alias cpuinfo='lscpu | ccze -A'                                                                    #CPU info
alias pscpu='echo "Top Processes accesing CPU  " | pv -qL 10 && ps auxf | sort -nr -k 3 | ccze -A' # Process eating CPU

# Terminal Clock
alias tclock="while sleep 1;do tput sc;tput cup 0 $(($(tput cols)-29));date;tput rc;done &"

# Remove offending key line $1
function rmkeyssh() { sed -i "${1}d" ~/.ssh/known_hosts; }

## DICTIONARY FUNCTIONS ##
dwordnet () { curl dict://dict.org/d:${1}:wn; }
dacron () { curl dict://dict.org/d:${1}:vera; }
djargon () { curl dict://dict.org/d:${1}:jargon; }
dfoldoc () { curl dict://dict.org/d:${1}:foldoc; }
dthesaurus () { curl dict://dict.org/d:${1}:moby-thes; }

#change the title of a terminal
function title(){
case "$TERM" in
	*)
		echo -en "\033]0;$*\a"
		;;
esac
}


## Website Commands ##
cmdfu() { curl "http://www.commandlinefu.com/commands/matching/$(echo "$@" | sed 's/ /-/g')/$(echo -n $@ | base64)/plaintext" ;}
down4me() { curl -s "http://www.downforeveryoneorjustme.com/$1" | sed '/just you/!d;s/<[^>]*>//g';}

# Swap 2 files around, if they exist (from Uzi's bashrc).
function swap() {
 # Create temp file
 local TMPFILE=tmp.$$
 # Check availability of files
 [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
 [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
 [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1
 # Move it
 mv "$1" $TMPFILE
 mv "$2" "$1"
 mv $TMPFILE "$2"
}

# Handy Extract Program
function extract() {
    if [ -f "$1" ] ; then
        case "$1" in
            *.tar.bz2)   tar xvjf "$1"     ;;
            *.tar.gz)    tar xvzf "$1"     ;;
            *.bz2)       bunzip2 "$1"      ;;
            *.rar)       unrar x "$1"      ;;
            *.gz)        gunzip "$1"       ;;
            *.tar)       tar xvf "$1"      ;;
            *.tbz2)      tar xvjf "$1"     ;;
            *.tgz)       tar xvzf "$1"     ;;
            *.zip)       unzip "$1"        ;;
            *.Z)         uncompress "$1"   ;;
            *.7z)        7z x "$1"         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# tab completion for ssh hosts
SSH_COMPLETE=( $(cat ~/.ssh/known_hosts | \
cut -f 1 -d ' ' | \
sed -e s/,.*//g | \
uniq | \
egrep -v ^[0123456789]) )
complete -o default -W "${SSH_COMPLETE[*]}" ssh
complete -o default -W "${SSH_COMPLETE[*]}" root

# Auto-complete bticket with the ticket numbers
#TICKET_COMPLETE=( $(/usr/bin/ls -1 ~/Bacula/ticket) )
#complete -o default -W "${TICKET_COMPLETE[*]}" bticket

# auto completion for sudo whereis and man
complete -cf sudo
complete -cf whereis
complete -cf man

battery_status() {

  BATTERY=/sys/class/power_supply/BAT0
  CHARGE=`cat $BATTERY/capacity`
  BATSTATE=`cat $BATTERY/status`

  # Colors for humans
  NON='\033[00m'
  BLD='\033[01m'
  RED='\033[01;31m'
  GRN='\033[01;32m'
  YEL='\033[01;33m'

  COLOUR="$RED"

  case "${BATSTATE}" in
   'Charged')
           BATSTT="$BLD=$NON"
           ;;
   'Charging')
           BATSTT="$BLD+$NON"
           ;;
   'Discharging')
           BATSTT="$BLD-$NON"
           ;;
  esac

  # prevent a charge of more than 100% displaying
  if [ "$CHARGE" -gt "99" ]
    then
      CHARGE=100
  fi

  # prevent an error if the battery is not in the laptop (e.g. you have two and take out the primary)
  STATE=`cat $BATTERY/present`
  if [ "$STATE" == '0' ]
    then
      echo -e "${RED}nobat"
      exit
  fi

  if [ "$CHARGE" -gt "15" ]
    then
      COLOUR="$YEL"
  fi

  if [ "$CHARGE" -gt "30" ]
    then
      COLOUR="$GRN"
  fi
  echo -e "${BATSTT}${COLOUR}${CHARGE}%${NON}"
}

if [ -f /usr/local/lib/python2.7/dist-packages/powerline/bindings/bash/powerline.sh ]; then
source /usr/local/lib/python2.7/dist-packages/powerline/bindings/bash/powerline.sh
fi


alias please='sudo $(fc -ln -1)'
