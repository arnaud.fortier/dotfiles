" Sets how many lines of history VIM has to remember
set history=300
set list
" Enable filetype plugin
" filetype plugin on
" filetype indent on

" Uncomment the following to have Vim jump to the last position when                                                       
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" 256 Colors
set t_Co=256

" Set to auto read when a file is changed from the outside
set autoread

"set number          " show line numbers
set ignorecase      " ignore case when searching
set hlsearch        "Highlight search things
set showmatch 		"Show matching bracets when text indicator is over them
set mat=2 			"How many tenths of a second to blink
set title           " show title in console title bar
set ttyfast         " smoother changes
"set ls=2            " allways show status line
set tabstop=4       " numbers of spaces of tab character
set shiftwidth=4    " numbers of spaces to (auto)indent
set ruler           " show the cursor position all the time
set notitle
set number
""""""""""""""""""""""""""""""
" => Statusline
""""""""""""""""""""""""""""""
" Always hide the statusline
"set laststatus=2

" Powerline
set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
" Always show statusline
set laststatus=2
" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256

" Format the statusline
set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{CurDir()}%h\ \ \ Line:\ %l/%L:%c

"Please change with your home
function! CurDir()
    let curdir = substitute(getcwd(), '/home/warnaud', "~/", "g")
    return curdir
endfunction

syntax enable "Enable syntax hl

" Default encoding
set encoding=utf8

" Spelling 
try
    lang en_US
catch
endtry

set ffs=unix,dos,mac "Default file types

" Spell check for those files only
autocmd BufRead,BufNewFile *.tex,*.txt setlocal spell

" Theme
set background=dark
let g:solarized_termcolors=256
colorscheme solarized
