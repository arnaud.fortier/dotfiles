# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt appendhistory autocd beep extendedglob nomatch notify SHARE_HISTORY
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/warnaud/.zshrc'

autoload -Uz compinit
compinit -i
# End of lines added by compinstall
source ~/powerlevel10k/powerlevel10k.zsh-theme


# Overload actual commands
alias grep='grep --color=auto'
alias dmesg='dmesg -HL'
alias vi='vim'
alias ls='ls -G'
alias history="history 0"
alias sysupgrade='sudo apt update &&sudo apt upgrade'


# New aliases
alias fifo="echo $1 > ~/FIFO"
alias lc="ls -al | tr -s \" \" | cut -d\" \" -f3,4,9 | column -t" 
alias ll="ls -altrh | tr -s \" \" | cut -d\" \" -f6,7,8,3,4,5,9 | column -t" 
alias learning="whatis \$(compgen -c) 2>/dev/null | sort | less"
alias da='date "+%A, %B %d, %Y [%T]"'
alias horloge="watch -n1 -t \"date '+%a %d %m/%Y - %H:%M:%S' | toilet -f future\""
alias please='sudo $(fc -ln -1)'

## FileSystem Aliases ##
alias lsl='ls -lh | ccze -A'          # list files, dirs
alias usage='du -ch | grep total'     # size of current dir 
alias dut="du -h -c -d 1"             # lst with size of elements in current dir
alias size='sudo du -hsx * | sort -rh | head -10 | ccze -A'         # list 10 dir/files by size
alias sizevar='sudo du -a /var | sort -n -r | head -n 10 | ccze -A' # list 10 dir by size
alias sizehome="sudo du -a ~/ | sort -n -r | head -n 10 | ccze -A"  # list 10 dir by size
alias lsbig="echo listing files & directories by size | pv -qL 10 && ls -lSrh | ccze -A"    # sort files dirs by size
alias listmod="ls -ltr | ccze -A"     # list modified files
alias listf="ls -F | ccze -A"         # list alphabeticaly
alias findit="sudo find / -name"      # find files
alias upd60="find . -mmin -60"        # updated files currentdir for last 60mins
alias rtupd60=" find / -mtime -1"     # updated files in / for last 60mins

## Network Aliases
alias netlisten='lsof -i -P | grep LISTEN | ccze -A'   #listening ports
alias nstat="netstat -p TCP -eWc | ccze -A"            #netstat
alias nstato="netstat -tuael --numeric-hosts --numeric-ports | ccze -A"   #netstat
alias ports='netstat -tulanp | ccze -A'  #netstat
alias speeddown="echo Displaying Download Speed Graph| pv -qL 10 && speedometer -rx eth0"
alias speedup="echo Displaying Upload Speed Graph | pv -qL 10 && speedometer -tx eth0"
alias speedrw="echo Show R/W speeds | pv -qL 10 && dd bs=1000000 count=100 if=/dev/zero of=testfile & speedometer testfile"

## System Aliases ##
alias h='history | ccze -A'           # Nice history
alias sourcebash="source ~/.bashrc"   # source .bashrc
alias inxif="inxi -F | ccze -A"       # System information
# Drives
alias uuid="echo â†’ Listing this system \($(hostname)\) UUID | pv -qL 10 && ls /dev/disk/by-uuid/ -alh | ccze -A" # UUID list
alias drives="echo â†’ Listing connected drives on \($(hostname)\) | pv -qL 10 && lsblk -f | ccze -A"              # list hdds, uuid's
# Systemd
alias kernelmsg="sudo journalctl -f _TRANSPORT=kernel | ccze -A"                  # kernel messages
alias bootmsg="echo â†’ Boot Messages | pv -qL 10 && sudo journalctl -b | ccze -A"  # boot messages
alias systemdmsg="sudo journalctl -f --since \" `date +\"%Y-%m-%d\"`\" | ccze -A"             # dmesg
alias blame="systemd-analyze blame | ccze -A"                                     # WTF slowed down the boot
alias boot="echo â†’ Boot Time | pv -qL 10 && systemd-analyze | ccze -A"            # Boot time
alias units="echo â†’ Listing Units (Systemd) | pv -qL 10 && systemctl list-units | ccze -A" # List of Units
alias errors="echo â†’ Systemd Journal Errors | pv -qL 10 && journalctl -b -p err | ccze -A" # Errors
# Ram
alias meminfo='echo "RAM Information   " | pv -qL 10 &&free -m -l -t | ccze -A'                    # RAM information
alias psmem='echo "Top Processes accesing RAM  " | pv -qL 10 && ps auxf | sort -nr -k 4 | ccze -A' # Process eating RAM
# CPU
alias cpuinfo='lscpu | ccze -A'                                                                    #CPU info
alias pscpu='echo "Top Processes accesing CPU  " | pv -qL 10 && ps auxf | sort -nr -k 3 | ccze -A' # Process eating CPU

# Terminal Clock
alias tclock="while sleep 1;do tput sc;tput cup 0 $(($(tput cols)-29));date;tput rc;done &"

# Python 3 and pip
if command -v pyenv 1>/dev/null 2>&1; then
 eval "$(pyenv init -)"
fi
#alias python=/usr/local/bin/python3
#alias pip=/usr/local/bin/pip3

# Remove offending key line $1
function rmkeyssh() { 
#echo "sed -i '"${1}"d' ~/.ssh/known_hosts" 
perl -pi -e 's/\Q$_// if ($. == ${1});' ~/.ssh/known_hosts
}

## DICTIONARY FUNCTIONS ##
dwordnet () { curl dict://dict.org/d:${1}:wn; }
dacron () { curl dict://dict.org/d:${1}:vera; }
djargon () { curl dict://dict.org/d:${1}:jargon; }
dfoldoc () { curl dict://dict.org/d:${1}:foldoc; }
dthesaurus () { curl dict://dict.org/d:${1}:moby-thes; }


#change the title of a terminal
function title(){
case "$TERM" in
    *)
        echo -en "\033]0;$*\a"
        ;;
esac
}

## Website Commands ##
cmdfu() { curl "http://www.commandlinefu.com/commands/matching/$(echo "$@" | sed 's/ /-/g')/$(echo -n $@ | base64)/plaintext" ;}
down4me() { curl -s "http://www.downforeveryoneorjustme.com/$1" | sed '/just you/!d;s/<[^>]*>//g';}

# Swap 2 files around, if they exist (from Uzi's bashrc).
function swap() {
 # Create temp file
 local TMPFILE=tmp.$$
 # Check availability of files
 [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
 [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
 [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1
 # Move it
 mv "$1" $TMPFILE
 mv "$2" "$1"
 mv $TMPFILE "$2"
}

# Handy Extract Program
function extract() {
    if [ -f "$1" ] ; then
        case "$1" in
            *.tar.bz2)   tar xvjf "$1"     ;;
            *.tar.gz)    tar xvzf "$1"     ;;
            *.bz2)       bunzip2 "$1"      ;;
            *.rar)       unrar x "$1"      ;;
            *.gz)        gunzip "$1"       ;;
            *.tar)       tar xvf "$1"      ;;
            *.tbz2)      tar xvjf "$1"     ;;
            *.tgz)       tar xvzf "$1"     ;;
            *.zip)       unzip "$1"        ;;
            *.Z)         uncompress "$1"   ;;
            *.7z)        7z x "$1"         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}




export PATH="/usr/local/sbin:$PATH"
source /usr/local/share/zsh-history-substring-search/zsh-history-substring-search.zsh
export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/usr/local/share/zsh-syntax-highlighting/highlighters
source ~/git/oh-my-zsh/plugins/zsh-navigation-tools/zsh-navigation-tools.plugin.zsh
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

export MONO_GAC_PREFIX="/usr/local"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
